
#pygame.display.update()clock.tick(120)
# pipe_list.append(create_pipe()) print(pipe_list)

import pygame
import random
import sys
#pygame.SysFont.init()

#import os
#all_fonts = pygame.font.get_fonts()
#font = pygame.font.Font(None, 15)
#font = pygame.font.SysFont("none", 72)
#font = pygame.font.Font("04B_19.TTF", 72)
#pygame.font.init()
#pygame.SysFont.init()
'''
def __init__(self):
    pygame.sprite.Sprite.__init__(self)
    self.font = pygame.font.SysFont("none", 30)
    self.text = "04B_19.TTF"
    self.center = (320, 240)
'''
'''
def initsysfonts_win32():
    try:
        import _winreg
    except ImportError:
        import winreg as _winreg

    if 'WINDIR' in os.environ:
        windir = os.environ['WINDIR']
    elif 'windir' in os.environ:
        windir = os.environ['windir']
    else:
        windir = "C:\\Windows\\"

'''

'''
def create_aliases():
    aliases = (
        ('monospace', 'misc-fixed', 'courier', 'couriernew', 'console',
         'fixed', 'mono', 'freemono', 'bitstreamverasansmono',
         'verasansmono', 'monotype', 'lucidaconsole'),
        ('sans', 'arial', 'helvetica', 'swiss', 'freesans',
         'bitstreamverasans', 'verasans', 'verdana', 'tahoma'),
        ('serif', 'times', 'freeserif', 'bitstreamveraserif', 'roman',
         'timesroman', 'timesnewroman', 'dutch', 'veraserif',
         'georgia'),
        ('wingdings', 'wingbats'),
    )
    for set in aliases:
        found = None
        fname = None
        for name in set:
            if name in Sysfonts:
                found = Sysfonts[name]
                fname = name
                break
        if not found:
            continue
        for name in set:
            if name not in Sysfonts:
                Sysalias[name] = found


Sysfonts = {}
Sysalias = {}

#initialize it all, called once
def initsysfonts():
    if sys.platform == 'win32':
        fonts = initsysfonts_win32()
    elif sys.platform == 'darwin':
        fonts = initsysfonts_darwin()
    else:
        fonts = initsysfonts_unix()
    Sysfonts.update(fonts)
    create_aliases()
    if not Sysfonts: #dummy so we don't try to reinit
        Sysfonts[None] = None
'''

'''
class SysFont(object):
    def __init__(self, name, size):
        self._font = NSFont.fontWithName_size_(name, size)
        self._isBold = False
        self._isOblique = False
        self._isUnderline = False
        self._family = name
        self._size = size
        self._setupFont()

    def _setupFont(self):
        name = self._family
        if self._isBold or self._isOblique:
            name = '%s-%s%s' % (
                name,
                self._isBold and 'Bold' or '',
                self._isOblique and 'Oblique' or '')
        self._font = NSFont.fontWithName_size_(name, self._size)
        print(name, self._font)
        if self._font is None:
            if self._isBold:
                self._font = NSFont.boldSystemFontOfSize(self._size)
            else:
                self._font = NSFont.systemFontOfSize_(self._size)

    def get_ascent(self):
        return self._font.ascender()

    def get_descent(self):
        return -self._font.descender()

    def get_bold(self):
        return self._isBold

    def get_height(self):
        return self._font.defaultLineHeightForFont()

    def get_italic(self):
        return self._isOblique

    def get_linesize(self):
        pass

    def get_underline(self):
        return self._isUnderline

    def set_bold(self, isBold):
        if isBold != self._isBold:
            self._isBold = isBold
            self._setupFont()

    def set_italic(self, isOblique):
        if isOblique != self._isOblique:
            self._isOblique = isOblique
            self._setupFont()

    def set_underline(self, isUnderline):
        self._isUnderline = isUnderline



    def render(self, text, antialias, forecolor, backcolor=(0, 0, 0, 255)):
        size = self.size(text)
'''



def draw_floor():
	screen.blit(floor_surface,(floor_x_pos,900))
	screen.blit(floor_surface,(floor_x_pos + 576,900))

def create_pipe():
	random_pipe_pos = random.choice(pipe_height)
	bottom_pipe = pipe_surface.get_rect(midtop = (700,random_pipe_pos))
	top_pipe = pipe_surface.get_rect(midbottom = (700,random_pipe_pos - 300))
	return bottom_pipe,top_pipe

def move_pipes(pipes):
	for pipe in pipes:
		pipe.centerx -= 5
	return pipes

def draw_pipes(pipes):
	for pipe in pipes:
		if pipe.bottom >= 1024:
			screen.blit(pipe_surface,pipe)
		else:
			flip_pipe = pygame.transform.flip(pipe_surface,False,True)
			screen.blit(flip_pipe,pipe)

def check_collision(pipes):
	for pipe in pipes:
		if bird_rect.colliderect(pipe):

			return False

	if bird_rect.top <= -100 or bird_rect.bottom >= 900:
		return False

	return True

def rotate_bird(bird):
	new_bird = pygame.transform.rotozoom(bird,-bird_movement * 3,1)
	return new_bird

def bird_animation():
	new_bird = bird_frames[bird_index]
	new_bird_rect = new_bird.get_rect(center = (100,bird_rect.centery))
	return new_bird,new_bird_rect

def score_display(game_state):
	if game_state == 'main_game':
		score_surface = game_font.render(str(int(score)),True,(255,255,255))
		score_rect = score_surface.get_rect(center = (288,100))
		screen.blit(score_surface,score_rect)
	if game_state == 'game_over':
		score_surface = game_font.render(f'Score: {int(score)}' ,True,(255,255,255))
		score_rect = score_surface.get_rect(center = (288,100))
		screen.blit(score_surface,score_rect)

		high_score_surface = game_font.render(f'High score: {int(high_score)}',True,(255,255,255))
		high_score_rect = high_score_surface.get_rect(center = (288,850))
		screen.blit(high_score_surface,high_score_rect)

def update_score(score, high_score):
	if score > high_score:
		high_score = score
	return high_score


def game_font(self):
    import pygame.font
    pygame.font.init()
    arial = pygame.font.SysFont('Arial', 40)
'''
def score_display(game_state, font = pygame.font.Font("04B_19.TTF", 72)):
    if game_state == "main_game":
        score_surface = font.render(str(int("Score", True, (255, 255, 255))))
        score_rect = score_surface.get_rect(center=(288, 100))
        screen.blit(score_surface,score_rect)
        font = pygame.font.Font("04B_19.TTF", 72)
    if game_state == "game_over":
        score_surface = font.render(f'Score: {int(score)}', True, (255, 255, 255))
        score_rect = score_surface.get_rect(center=(288, 100))
        screen.blit(score_surface, score_rect)

        high_score_surface = font.render(f'high_Score: {int(high_score)}', True, (255, 255, 255))
        high_score_rect = high_score_surface.get_rect(center=(288, 850))
        font = pygame.font.Font("04B_19.TTF", 72)

def update_score(score, high_score):
    if score > high_score:
        high_score = score
    return high_score

'''

#score_rect = score_surface.get_rect(center=(288, 100))
#screen.blit(score_surface, score_rect)


pygame.mixer.pre_init(frequency=44100, size=16, channels=1, buffer=512)
pygame.init()
screen = pygame.display.set_mode((576, 1024))
clock = pygame.time.Clock()
#game_font = pygame.font.Font("04B_19.TTF", 72)
font = pygame.font.SysFont("none", 72)

#game variables
gravity = 0.25
bird_movement = 0
game_active = True
score = 0
high_score = 0

game_over_surface = pygame.transform.scale2x(pygame.image.load("gameover.png").convert_alpha())
game_over_rect = game_over_surface.get_rect(center=(288, 512))

flap_sound = pygame.mixer.Sound('sfx_wing.wav')

bg_surface = pygame.image.load("background-day.png").convert()
bg_surface = pygame.transform.scale2x(bg_surface)

floor_surface = pygame.image.load("base.png").convert()
floor_surface = pygame.transform.scale2x(floor_surface)
floor_x_pos = 0

bird_downflap = pygame.image.load("bluebird-downflap.png").convert_alpha()
bird_midflap = pygame.image.load("bluebird-midflap.png").convert_alpha()
bird_upflap = pygame.image.load("bluebird-upflap.png").convert_alpha()
bird_frames = [bird_downflap, bird_midflap, bird_upflap]
bird_index = 0
bird_surface = bird_frames[bird_index]
bird_rect = bird_surface.get_rect(center=(100, 512))

BIRDFLAP = pygame.USEREVENT + 1
pygame.time.set_timer(BIRDFLAP, 200)

#bird_surface = pygame.image.load("bluebird-midflap.png").convert_alpha()
#bird_surface = pygame.transform.scale2x(bird_surface)
#bird_rect = bird_surface.get_rect(center = (100, 512))

pipe_surface = pygame.image.load("pipe-green.png")
pipe_surface = pygame.transform.scale2x(pipe_surface)

pipe_list = []
SPAWNPIPE = pygame.USEREVENT
pygame.time.set_timer(SPAWNPIPE,1200)
pipe_height = [400, 600, 800]

while True:
    for event in pygame.event.get():

        if event.type == pygame.QUIT:
            pygame.quit()
            sys.exit()
        if event.type == pygame.KEYDOWN:
            if event.key == pygame.K_SPACE:
                bird_movement = 0
                bird_movement -= 12
                flap_sound.play()
            if event.key == pygame.K_SPACE and game_active == False:
                game_active = True
                pipe_list.clear()
                bird_rect.center = (100, 512)
                bird_movement = 0

        if event.type == SPAWNPIPE:
            pipe_list.extend(create_pipe())

        if event.type == BIRDFLAP:
            if bird_index < 2:
                bird_index += 1
        else:
            bird_index = 0

        bird_surface, bird_rect = bird_animation()

    screen.blit(bg_surface, (0, 0))

    if game_active:
            #bird
            bird_movement += gravity
            rotated_bird = rotate_bird(bird_surface)
            bird_rect.centery += bird_movement
            screen.blit(rotated_bird, bird_rect)
            game_active = check_collision(pipe_list)

        #pipes
            pipe_list = move_pipes(pipe_list)
            draw_pipes(pipe_list)
            score_display()
            score += 0.01
            score_display("main_game")

    else:
        screen.blit(game_over_surface, game_over_rect)
        high_score = update_score(score, high_score)
        score_display("game_over")

    #floor
    floor_x_pos -= 1
    draw_floor()
    if floor_x_pos <= -576:
        floor_x_pos = 0



pygame.display.update()
clock.tick(120)